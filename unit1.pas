unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Grids,
  StdCtrls, ComCtrls, DbCtrls, maskedit, ExtCtrls, windows;
  type
	rekord = record
ID :Integer;
film :String[30];
ilosc : word;
Nosnik : String[30];
cena : real;
faktura : boolean;
kraj : String[30];
wystawil : String[30];

end;

type
    poczateklan = record

      pocz : Integer;
      waga : Integer;
      nazwa : String;
      kolumna : Integer;

    end;

type
    poczatekinw = record

    waga : integer;
    nazwa : string;
    lista : array of Integer;
    kolumna : integer;

    end;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    ComboBox2: TComboBox;
    kolor: TLabel;
    Label1: TLabel;
    Label10: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    liniowe: TButton;
    Edit8: TEdit;
    Tak: TRadioButton;
    Nie: TRadioButton;
    Usun: TButton;
    Edit6: TEdit;
    Losuj: TButton;
    Edit5: TEdit;
    Wczytaj: TButton;
    Zapisz: TButton;
    Cena: TLabel;
    ComboBox1: TComboBox;
    Edit2: TEdit;
    Edit4: TEdit;
    Nosnik: TLabel;
    Dodaj: TButton;
    Edit7: TEdit;
    GroupBox1: TGroupBox;
    kraj: TLabel;
    Edit1: TEdit;
    Edit3: TEdit;
    Faktura: TLabel;
    Ilosc_szt: TLabel;
    film: TLabel;
    tabelka: TStringGrid;

    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);



    procedure DodajClick(Sender: TObject);
    procedure DodawanieClick(Sender: TObject);
    procedure Edit5Change(Sender: TObject);
    procedure Edit8Change(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GroupBox1Click(Sender: TObject);
    procedure Label3Click(Sender: TObject);
    procedure Label4Click(Sender: TObject);
    procedure Label5Click(Sender: TObject);

    procedure linioweClick(Sender: TObject);



    procedure LosujClick(Sender: TObject);
    procedure MaskEdit2EditingDone(Sender: TObject);
    procedure SortujClick(Sender: TObject);
    procedure UsunClick(Sender: TObject);

    procedure ZapiszClick(Sender: TObject);
    procedure WczytajClick(Sender: TObject);
    procedure szukajliniowo(Form1 : integer);
    procedure dorekordu(nr : integer);
    procedure zrekordu(nr : integer);
    procedure Quicksort (l,r:Integer; poczym : Integer);
    procedure wyczysc();
    procedure szukajbinarnie(Form1 : Integer);
    procedure kryt(var kryterium : rekord; Form1 :Integer);
    procedure wyszukaj(lewo, prawo, poczym: integer; var pocz,kon : integer; kryterium : rekord );
    procedure wyswietlwszystko();
    procedure kopiujdotablicy();
  private
    { private declarations }
  public


  end;

var
  Form1: TForm1;
  ID : Integer;
  tab : array of rekord;
  lancuchowe : array of poczateklan; //Tablica poczatkow lancuchowego
  inwersyjne : array of poczatekinw;  //Tablica poczatkow inwersyjnego
  lancuch : array [0..7] of array of integer;
    start1, stop1: int64;
  PerformanceFrequency: int64;

implementation

{$R *.lfm}

{ TForm1 }




procedure TForm1.FormActivate(Sender: TObject);
begin

tabelka.Cells[0,0] := 'LD';
  tabelka.Cells[1,0] := 'Film';
  tabelka.Cells[2,0] := 'Rok';
  tabelka.Cells[3,0] := 'Nosnik';
  tabelka.Cells[4,0] := 'Cena';
  tabelka.Cells[5,0] := 'Dostepny';
  tabelka.Cells[6,0] := 'kraj';
  tabelka.Cells[7,0] := 'Reżyser';
  tabelka.RowCount := 2;
     ID := 0;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
Label2.Visible := true;     //********
Label3.Visible := true;     //*****
//Label4.Visible := true;    //********
//Label5.Visible := true    //********

ComboBox2.Items.Clear;             //Delete all existing choices
ComboBox2.Items.Add('ID');        //Add an choice
ComboBox2.Items.Add('Film');
ComboBox2.Items.Add('Ilosc');
ComboBox2.Items.Add('Kolor');
ComboBox2.Items.Add('Cena');
ComboBox2.Items.Add('Faktura');
ComboBox2.Items.Add('kraj');
ComboBox2.Items.Add('Wystawiał');

end;

procedure TForm1.GroupBox1Click(Sender: TObject);
begin

end;

procedure TForm1.Label3Click(Sender: TObject);
begin

end;

procedure TForm1.Label4Click(Sender: TObject);
begin

end;

procedure TForm1.Label5Click(Sender: TObject);
begin

end;

procedure nowyl(war : Ansistring; nr, ost : integer);
begin
       //LANCUCHOWO
        setlength(lancuchowe, length(lancuchowe)+1);
     	lancuchowe[Length(lancuchowe)-1].nazwa := war;
        lancuchowe[Length(lancuchowe)-1].waga := 1;
        lancuchowe[Length(lancuchowe)-1].kolumna := nr;
        lancuchowe[Length(lancuchowe)-1].pocz := ost;
        setlength(lancuch[nr], length(lancuch[nr])+1);
        lancuch[nr, length(lancuch[nr])-1] := -1;

end;

procedure nowyi(war : Ansistring; nr, ost : integer);
begin

     	//INWERSYJNIE
        setlength(inwersyjne, length(inwersyjne)+1);
     	inwersyjne[Length(inwersyjne)-1].nazwa := war;
        inwersyjne[Length(inwersyjne)-1].waga := 1;
        inwersyjne[Length(inwersyjne)-1].kolumna := nr;
        setlength(inwersyjne[Length(inwersyjne)-1].lista, length(inwersyjne[Length(inwersyjne)-1].lista) +1);
        inwersyjne[Length(inwersyjne)-1].lista[length(inwersyjne[Length(inwersyjne)-1].lista)-1] := ost;
end;
//
function sprawdzl(war : Ansistring; nr, ost : integer) : boolean;
var i : integer;
begin


		for i := 0 to length(lancuchowe)-1 do
		begin
			if ((war = lancuchowe[i].nazwa) and (lancuchowe[i].kolumna = nr)) then
			begin
				//LANCUCHOWO
                                setlength(lancuch[nr], length(lancuch[nr])+1 );
				lancuch[nr, length(lancuch[nr])-1] := lancuchowe[i].pocz;
				lancuchowe[i].waga := lancuchowe[i].waga + 1;
				lancuchowe[i].pocz:= ost;

				result := false;
                                exit;
			end;

                end;

		result := true;
end;
function sprawdzi(war : Ansistring; nr, ost : integer) : boolean;
var i : integer;
begin


		for i := 0 to length(inwersyjne)-1 do
		begin
			if ((war = inwersyjne[i].nazwa) and (inwersyjne[i].kolumna = nr)) then
			begin
				//INWERSYJNIE
				inwersyjne[i].waga := inwersyjne[i].waga + 1 ;
                                setlength(inwersyjne[i].lista, length(inwersyjne[i].lista) +1);
				inwersyjne[i].lista[length(inwersyjne[i].lista)-1] := ost;


				result := false;
                                exit;
			end;

                end;

		result := true;
end;
//
procedure tpl(rekord1 : rekord; ost : integer);
var war : Ansistring;
   i : integer;
begin
	               for i := 0 to 7 do begin

			if (i = 0) then war := IntToStr(rekord1.ID);
			if (i = 1)then  war := rekord1.film;
			if (i = 2)then  war := IntToStr(rekord1.ilosc);
			if (i = 3)then  war := rekord1.Nosnik;
			if (i = 4)then  war := FloattoStr(rekord1.cena);
			if (i = 5)then  if (rekord1.faktura = true) then  war := 'TAK' else war := 'NIE';
			if (i = 6)then  war := rekord1.kraj;
			if (i = 7)then  war := rekord1.wystawil;

			if (sprawdzl(war,  i, ost)) = true then begin
				nowyl(war, i, ost);

                        end;
                       //lancuchowo


		end;
end;
procedure tpi(rekord1 : rekord; ost : integer);
var war : Ansistring;
   i : integer;
begin
	               for i := 0 to 7 do begin

			if (i = 0) then war := IntToStr(rekord1.ID);
			if (i = 1)then  war := rekord1.film;
			if (i = 2)then  war := IntToStr(rekord1.ilosc);
			if (i = 3)then  war := rekord1.Nosnik;
			if (i = 4)then  war := FloattoStr(rekord1.cena);
			if (i = 5)then  if (rekord1.faktura = true) then  war := 'TAK' else war := 'NIE';
			if (i = 6)then  war := rekord1.kraj;
			if (i = 7)then  war := rekord1.wystawil;

			if (sprawdzi(war,  i, ost)) = true then begin
				nowyi(war, i, ost);
                         //inwersyjnie
                        end;



		end;
end;


 function porownaj (Form1,b : rekord; p : integer) : Integer;
begin

if p = 0 then begin
if Form1.ID < b.ID then result := 1; if Form1.ID > b.ID then result := 2; if Form1.ID = b.ID then result := 0; end;
if p = 1 then begin
if Form1.film < b.film then result := 1; if Form1.film > b.film then result := 2; if Form1.film = b.film then result := 0; end;
if p = 2 then begin
if Form1.ilosc < b.ilosc then result := 1; if Form1.ilosc > b.ilosc then result := 2; if Form1.ilosc = b.ilosc then result := 0; end;
if p = 3 then begin
if Form1.Nosnik < b.Nosnik then result := 1; if Form1.Nosnik > b.Nosnik then result := 2; if Form1.Nosnik = b.Nosnik then result := 0; end;
if p = 4 then begin
if Form1.cena < b.cena then result := 1; if Form1.cena > b.cena then result := 2; if Form1.cena = b.cena then result := 0; end;
if p = 5 then begin
if Form1.faktura < b.faktura then result := 1; if Form1.faktura > b.faktura then result := 2; if Form1.faktura = b.faktura then result := 0; end;
if p = 6 then begin
if Form1.kraj < b.kraj then result := 1; if Form1.kraj > b.kraj then result := 2; if Form1.kraj = b.kraj then result := 0; end;
if p = 7 then begin
if Form1.wystawil < b.wystawil then result := 1; if Form1.wystawil > b.wystawil then result := 2; if Form1.wystawil = b.wystawil then result := 0; end;
end;

procedure TForm1.kopiujdotablicy();
var i : Integer;
begin
if Button1.Enabled = true then exit;
SetLength(tab, tabelka.RowCount-1);
  for i:= 1 to tabelka.RowCount-1 do
  dorekordu(i);

end;
procedure TForm1.wyswietlwszystko();
var i : Integer;
begin
wyczysc;
QuickSort(0,Length(tab)-1,0);
for i:=0 to Length(tab)-1 do begin
zrekordu(i);
 tabelka.RowCount := tabelka.RowCount+1;
end;
  if tabelka.RowCount > 2 then
tabelka.RowCount := tabelka.RowCount-1;

  Button1.Enabled := false;

end;

procedure TForm1.linioweClick(Sender: TObject);

begin
try
//kopiujdotablicy;
except
ShowMessage('Baza pusta!');
exit;
end;
if length(tab) <1 then ShowMessage('Baza pusta!');


if (ComboBox2.ItemIndex <> -1) then begin
Button1.Enabled := true;
if CheckBox1.Checked = false then                                //przycisk Wyszukaj
szukajliniowo(ComboBox2.ItemIndex)
else
szukajbinarnie(ComboBox2.ItemIndex)
end
else
ShowMessage('Wybierz kryterium wyszukiwania !');



end;

procedure TForm1.kryt(var kryterium : rekord; Form1 :Integer);
begin

if Form1 = 0 then kryterium.ID := StrToInt(Edit8.Text);
if Form1 = 1 then kryterium.film := Edit8.Text;
if Form1 = 2 then kryterium.ilosc := StrToInt(Edit8.Text);
if Form1 = 3 then kryterium.Nosnik := Edit8.Text;                           // ustawienie rekordu do porownywania
if Form1 = 4 then kryterium.cena := StrToFloat(Edit8.Text);
if Form1 = 5 then kryterium.faktura := CheckBox2.Checked;
if Form1 = 6 then kryterium.kraj := Edit8.Text;
if Form1 = 7 then kryterium.wystawil := Edit8.Text;

end;

procedure TForm1.szukajbinarnie(Form1 : Integer);        //binarnie
var kryterium : rekord;
     pocz, kon, i : Integer;


begin
pocz:=0;kon:=-1;
try
kryt(kryterium, Form1);
except
    ShowMEssage('Podaj poprawne kryterium !');
   exit;
  end;
                    //ustawieni kryterium
 QueryPerformanceFrequency(PerformanceFrequency);
QueryPerformanceCounter(start1);

Quicksort (0,Length(tab)-1, Form1);//sortowanie
QueryPerformanceCounter(stop1);
Label5.Caption:= Floattostr((stop1-start1) / (PerformanceFrequency/1000)) + ' ms';

QueryPerformanceFrequency(PerformanceFrequency);
QueryPerformanceCounter(start1);

wyszukaj(0, Length(tab)-1, Form1, pocz, kon , kryterium);       //wyszukiwanie binarne

wyczysc;
for i := pocz to kon do                                      //wypisanie znalezionych
begin
zrekordu(i);                                   //*********************
tabelka.RowCount := tabelka.RowCount+1;         //*******************
end;
 QueryPerformanceCounter(stop1);
Label2.Caption:= Floattostr((stop1-start1) / (PerformanceFrequency/1000)) + ' ms';

  if tabelka.RowCount > 2 then
tabelka.RowCount := tabelka.RowCount-1;


end;

procedure TForm1.szukajliniowo(Form1 : integer);     //linowo
var i, h : integer;
temp : rekord;
begin


  QueryPerformanceFrequency(PerformanceFrequency);
QueryPerformanceCounter(start1);
try
kryt(temp, Form1);
except
    ShowMEssage('Podaj poprawne kryterium !');
    exit;
  end;
wyczysc;
for i := 0 to Length(tab)-1 do
begin
if porownaj(temp, tab[i],Form1) = 0 then
begin
zrekordu(i);                              //**********************
tabelka.RowCount := tabelka.RowCount+1;    //***************************
end;

end;
QueryPerformanceCounter(stop1);
Label2.Caption:= Floattostr((stop1-start1) / (PerformanceFrequency/1000)) + ' ms';
  if tabelka.RowCount > 2 then
tabelka.RowCount := tabelka.RowCount-1;

  end;

procedure TForm1.wyczysc();
var i : integer;
begin

for i:= 1 to tabelka.RowCount-1 do
tabelka.Rows[i].Clear();

tabelka.RowCount := 2;

end;

procedure TForm1.dorekordu(nr : integer);               // z tablicy do rekordu
begin


tab[nr-1].ID := StrToInt(tabelka.Cells[0, tabelka.RowCount -1]);
tab[nr-1].film := tabelka.Cells[1, tabelka.RowCount -1];
tab[nr-1].ilosc := StrToInt(tabelka.Cells[2, tabelka.RowCount -1]);
tab[nr-1].Nosnik := tabelka.Cells[3, tabelka.RowCount -1];
tab[nr-1].cena := StrToFloat(tabelka.Cells[4, tabelka.RowCount -1]) ;
if tabelka.Cells[5, tabelka.RowCount -1] = 'Tak' then tab[nr-1].faktura := true  else  tab[nr-1].faktura := false;
tab[nr-1].kraj := tabelka.Cells[6, tabelka.RowCount -1];
tab[nr-1].wystawil := tabelka.Cells[7, tabelka.RowCount -1];

end;

procedure TForm1.zrekordu(nr : integer);             // z rekordu do tablicy
begin
if tab[nr].ID = -1 then begin tabelka.RowCount := tabelka.RowCount -1; exit; end;
tabelka.Cells[0, tabelka.RowCount-1] := InttoStr(tab[nr].ID);
tabelka.Cells[1, tabelka.RowCount-1] := tab[nr].film;
tabelka.Cells[2, tabelka.RowCount-1] := InttoStr(tab[nr].ilosc);
tabelka.Cells[3, tabelka.RowCount-1] := tab[nr].Nosnik;
tabelka.Cells[4, tabelka.RowCount-1] := FloatToStr(tab[nr].cena);
if  tab[nr].faktura then tabelka.Cells[5, tabelka.RowCount-1] := 'Tak' else tabelka.Cells[5, tabelka.RowCount-1] := 'Nie';
tabelka.Cells[6, tabelka.RowCount-1] := tab[nr].kraj;
tabelka.Cells[7, tabelka.RowCount-1] := tab[nr].wystawil;

end;

procedure TForm1.DodajClick(Sender: TObject);
   var
      liczba: integer;
      i, idx , k:integer;
   begin
   if (length(edit1.text)<1 ) then exit;
   if (length(edit7.text)<1 ) then exit;
   if (length(edit3.text)<1 ) then exit;


       if ((Tak.Checked = false) and  (Nie.Checked = false) ) then exit;
  // if (length(Maskedit1.text)<1 ) then exit;
    if (length(Edit2.text)<1 ) then exit;


        if (length(Edit4.text)<1 ) then exit;
      try
     StrToInt(Edit4.Text);
    except ShowMessage('Podaj liczbe calkowita (ilosc sztuk)!'); exit; end;

    try
    StrtoFloat(edit2.text);
    except ShowMessage('Podaj prawidlowa cene !'); exit;
     end;


 ID:=ID+1;
 if tabelka.Cells[0, 1] <> '' then  tabelka.RowCount:=tabelka.RowCount+1;
    tabelka.Cells[0, tabelka.RowCount-1] := IntToStr(ID);
   tabelka.Cells[1, tabelka.RowCount-1]:= Edit1.Text;
tabelka.Cells[4, tabelka.RowCount-1]:= edit2.text;
   tabelka.Cells[6, tabelka.RowCount-1]:= Edit3.Text;
   for k:=0 to length(Edit7.text)    do
   if Edit7.text[k] <> ' ' then
   tabelka.Cells[7, tabelka.RowCount-1]:= tabelka.Cells[7, tabelka.RowCount-1] + Edit7.Text[k] else
      tabelka.Cells[7, tabelka.RowCount-1]:= tabelka.Cells[7, tabelka.RowCount-1] + Edit7.Text[k] ;
   tabelka.Cells[3, tabelka.RowCount-1]:= combobox1.Text;
   tabelka.Cells[2, tabelka.RowCount-1]:= edit4.Text;
   if (Nie.Checked) then tabelka.Cells[5,tabelka.RowCount-1] := 'Nie' else tabelka.Cells[5,tabelka.RowCount-1] := 'Tak';
   setlength(tab, length(tab)+1);
   dorekordu(length(tab));
   tpi(tab[length(tab)-1],length(tab)-1 );
   tpl(tab[length(tab)-1],length(tab)-1 );
      end;

procedure TForm1.DodawanieClick(Sender: TObject);
begin

end;

procedure TForm1.Edit5Change(Sender: TObject);
begin

end;

procedure TForm1.Edit8Change(Sender: TObject);
begin

end;


//


procedure TForm1.LosujClick(Sender: TObject);
var plik,plik2,plik3,plik4,plik5,plik6,plik7 : TextFile;
    imiona : array [0..626] of String;
    nazwiska : array [0..1026] of String;
     film1 : array [0..100] of String;
      kraj1 : array [0..19] of String;
       Nosnik1 : array [0..4] of String;
      szt: array [0..5] of string;
      cena1: array [0..5] of string;
      i,pocz, roz, k : integer;
begin
Randomize;
try
 StrToInt(Edit5.Text);
except ShowMessage('Podaj liczbe calkowita'); exit; end;

AssignFile(plik, 'imiona.txt');
if not FileExists('imiona.txt') then begin  ShowMessage('Brak wymaganego pliku !'); exit; end;
Reset(plik);

AssignFile(plik2, 'nazwiska.txt');
if not FileExists('nazwiska.txt') then begin  ShowMessage('Brak wymaganego pliku !'); Closefile(plik); exit; end;
Reset(plik2);
AssignFile(plik3, 'film.txt');
if not FileExists('film.txt') then begin  ShowMessage('Brak wymaganego pliku !'); Closefile(plik); exit; end;
Reset(plik3);
AssignFile(plik4, 'Nosnik.txt');
if not FileExists('Nosnik.txt') then begin  ShowMessage('Brak wymaganego pliku !');Closefile(plik); exit; end;
Reset(plik4);

AssignFile(plik5, 'kraj.txt');
if not FileExists('kraj.txt') then begin  ShowMessage('Brak wymaganego pliku !');Closefile(plik); exit; end;
Reset(plik5);

AssignFile(plik6, 'cena.txt');
if not FileExists('cena.txt') then begin  ShowMessage('Brak wymaganego pliku !');Closefile(plik); exit; end;
Reset(plik6);
AssignFile(plik7, 'szt.txt');
if not FileExists('szt.txt') then begin  ShowMessage('Brak wymaganego pliku !');Closefile(plik); exit; end;
Reset(plik7);


for i:=0 to 626 do
readln(plik, imiona[i]);


for i:=0 to 1023 do
readln(plik2, nazwiska[i]);
for i:=0 to 100 do
readln(plik3, film1[i]);
for i:=0 to 19 do
readln(plik5, kraj1[i]);
for i:=0 to 4 do
readln(plik4, Nosnik1[i]);
for i:=0 to 4 do
readln(plik6, cena1[i]);
for i:=0 to 4 do
readln(plik7, szt[i]);
pocz := Length(tab);

roz := length(tab);

setlength(tab, length(tab) + StrToInt(Edit5.Text));
for i:=0 to StrToInt(Edit5.Text)-1 do begin
ID:=ID+1;
if tabelka.Cells[0, 1] <> '' then  tabelka.RowCount:=tabelka.RowCount+1;
tabelka.Cells[0, tabelka.RowCount-1]:= InttoStr(ID);
tabelka.Cells[7, tabelka.RowCount-1] := imiona[random(625)] + ' ' + nazwiska[random(1022)];  //imie + nazwisko
tabelka.Cells[1, tabelka.RowCount-1] := film1[random(99)];
tabelka.Cells[6, tabelka.RowCount-1] := kraj1[random(19)];
tabelka.Cells[2, tabelka.RowCount-1] := szt[random(4)];
tabelka.Cells[4, tabelka.RowCount-1] := cena1[random(4)];
tabelka.Cells[3, tabelka.RowCount-1] := Nosnik1[random(4)];
//tabelka.Cells[5, tabelka.RowCount-1] := 'Tak';
if (random(2)  = 1) then  tabelka.Cells[5, tabelka.RowCount-1] := 'Tak' else    tabelka.Cells[5, tabelka.RowCount-1] := 'Nie';


dorekordu(roz+i+1);









end;
QueryPerformanceFrequency(PerformanceFrequency);
QueryPerformanceCounter(start1);
for k := 0 to length(tab)-1 do begin              //uzupelnianie tablic poczatkow
   tpl(tab[k],k );
end;

QueryPerformanceCounter(stop1);
Label4.Caption:= Floattostr((stop1-start1) / (PerformanceFrequency/1000)) + ' ms';

QueryPerformanceFrequency(PerformanceFrequency);
QueryPerformanceCounter(start1);

for k := 0 to length(tab)-1 do begin
   tpi(tab[k],k );                                    //uzupelnianie tablic poczatkow
end;

QueryPerformanceCounter(stop1);
Label3.Caption:= Floattostr((stop1-start1) / (PerformanceFrequency/1000)) + ' ms';
//tabelka.RowCount:=tabelka.RowCount-1;
Closefile(plik);
Closefile(plik2);
Closefile(plik3);
Closefile(plik4);
Closefile(plik5);
Closefile(plik6);
Closefile(plik7);
end;

procedure TForm1.MaskEdit2EditingDone(Sender: TObject);
begin

end;

procedure TForm1.SortujClick(Sender: TObject);
begin

end;

procedure TForm1.UsunClick(Sender: TObject);
 var
  i,j:integer;
  znaleziono:integer;
  usun1: String;
  begin
znaleziono := 0;
usun1 := Edit6.Text;
Edit6.Text := '';
for i:=1 to tabelka.RowCount-1  do
if tabelka.Cells[0,i] = usun1 then begin znaleziono := 1; break; end;

if znaleziono = 0 then begin  ShowMessage('Nie ma takiego elementu !'); exit; end;
tab[StrtoInt(tabelka.Cells[0,i])-1].ID := -1;
for  j := i to  tabelka.RowCount-2 do
tabelka.Rows[j] := tabelka.Rows[j+1];

if (tabelka.RowCount > 2) then tabelka.RowCount:=tabelka.rowcount-1 else tabelka.Rows[tabelka.RowCount-1].Clear;



  end;



procedure TForm1.WczytajClick(Sender: TObject);


var plik:File of rekord;
i,k : integer;
rek : rekord;

  begin
  i:=1;
  Assignfile(plik, 'baza');
  if not FileExists('baza') then begin ShowMessage('Nie ma pliku z baza !'); exit; end;
  Reset(plik);
  tabelka.RowCount:= 2;
 while (not eof(plik)) do begin
  read(plik, rek);

  tabelka.Cells[0, tabelka.RowCount-1] := IntToStr(rek.ID);
 tabelka.Cells[1, tabelka.RowCount-1] := rek.film ;
  tabelka.Cells[2, tabelka.RowCount-1] := IntToStr(rek.ilosc);
  tabelka.Cells[3, tabelka.RowCount-1] := rek.Nosnik;
  tabelka.Cells[4, tabelka.RowCount-1] := FloatToStr(rek.cena);
 if rek.faktura = true then tabelka.Cells[5,tabelka.RowCount-1] := 'Tak' else tabelka.Cells[5,tabelka.RowCount-1] := 'Nie';
  tabelka.Cells[6, tabelka.RowCount-1] := rek.kraj;
 tabelka.Cells[7, tabelka.RowCount-1] := rek.wystawil;
   tabelka.RowCount := tabelka.RowCount +1;

    setlength(tab, length(tab)+1);
   tab[length(tab)-1] := rek;

  end;

    for k := 0 to length(tab)-1 do begin

  tpi(tab[k],k );



    end;
  for k := 0 to length(tab)-1 do begin
     tpl(tab[k],k );
     end;

 tabelka.RowCOunt:=tabelka.RowCount -1;
 ID := rek.ID;
 Closefile(plik);
  end;



procedure TForm1.ZapiszClick(Sender: TObject);

  var plik : File of rekord;
      i : integer;
      rek : rekord;
  begin
  Assignfile(plik, 'baza');
  Rewrite(plik);
  for i := 1 to tabelka.RowCount-1 do begin
  rek.ID:=StrtoInt(tabelka.Cells[0,i]);
  rek.film:= tabelka.Cells[1,i];
  rek.ilosc := StrtoInt(tabelka.Cells[2,i]);
  rek.Nosnik := tabelka.Cells[3,i];
  rek.cena := StrtoFloat(tabelka.Cells[4,i]);
  if tabelka.Cells[5,i] = 'Tak' then rek.faktura:= true else rek.faktura := false;
  rek.kraj := tabelka.Cells[6,i] ;
  rek.wystawil := tabelka.Cells[7,i];

  write(plik, rek);
  end;
  Closefile(plik);
  end;





Procedure TForm1.Quicksort (l,r:Integer; poczym : Integer);   //sortowanie
Var
    i,j:Integer;
    pivot, b : rekord;
Begin
     pivot := tab[(l+r) shr 1];
     i := l ;
     j := r;
     repeat
          while porownaj(tab[i], pivot, poczym) = 1 do i:=i+1;
          while porownaj(tab[j], pivot, poczym) = 2 do j:=j-1;
          if i <= j then begin
          b := tab[i];
          tab[i] := tab[j];
          tab[j] := b;
          i:=i+1;
          j:=j-1;
          end;
     until  i>j;
        if l < j then Quicksort(l, j,poczym);
        if r > i then  Quicksort(i,r,poczym);
End;




procedure TForm1.Button1Click(Sender: TObject);
var i : integer;
begin
wyswietlwszystko;

end;

procedure TForm1.Button2Click(Sender: TObject);
var war : Ansistring;
   k,j, i, start : integer;
begin




Button1.Enabled := true;
  begin
tabelka.RowCount := 1;
QueryPerformanceFrequency(PerformanceFrequency);
QueryPerformanceCounter(start1);

if ComboBox2.ItemIndex = -1 then exit;


			if (ComboBox2.ItemIndex = 5) then  begin if (CheckBox2.Checked = true) then  war := 'TAK' else war := 'NIE'; end else   war := Edit8.Text;


for j := 0 to length(inwersyjne)-1 do   begin
			if (war = inwersyjne[j].nazwa) and (inwersyjne[j].kolumna = ComboBox2.ItemIndex) then
			begin
                          start := j;


                          break;
                          end;

end;

  end;

for i := 0 to length(inwersyjne[start].lista)-1 do begin
tabelka.RowCount := tabelka.RowCount +1;     //*********************
zrekordu(inwersyjne[start].lista[i]);       //*********************


end;
QueryPerformanceCounter(stop1);
Label2.Caption:= Floattostr((stop1-start1) / (PerformanceFrequency/1000)) + ' ms';
end;

procedure TForm1.Button3Click(Sender: TObject);
var war : Ansistring;
   j,k , i, start : integer;
begin



Button1.Enabled := true;
tabelka.RowCount := 1;
 QueryPerformanceFrequency(PerformanceFrequency);
QueryPerformanceCounter(start1);

if ComboBox2.ItemIndex = -1 then exit;


			if (ComboBox2.ItemIndex = 5) then  begin if (CheckBox2.Checked = true) then  war := 'TAK' else war := 'NIE'; end else   war := Edit8.Text;


for j := 0 to length(lancuchowe)-1 do   begin
			if (war = lancuchowe[j].nazwa) and (lancuchowe[j].kolumna = ComboBox2.ItemIndex) then
			begin
                          start := lancuchowe[j].pocz;


                          break;
                          end;

end;


while(true) do begin
tabelka.RowCount := tabelka.RowCount +1;     //*********************

zrekordu(start);               //*********************

start := lancuch[ComboBox2.ItemIndex, start];
if start = -1 then break;

end;
QueryPerformanceCounter(stop1);
Label2.Caption:= Floattostr((stop1-start1) / (PerformanceFrequency/1000)) + ' ms';

end;

procedure TForm1.Button4Click(Sender: TObject);
var i : integer;
begin
  showMessage(inttostr(length(tab)));

end;

procedure TForm1.CheckBox1Change(Sender: TObject);
begin

end;

procedure TForm1.ComboBox2Change(Sender: TObject);
begin

if not (ComboBox2.ItemIndex = 5) then
begin Edit8.Enabled := true ; Edit8.Visible:= true; CheckBox2.Visible := false end else begin Edit8.Visible := false; CheckBox2.Visible := true; CheckBox2.Checked := false; end;

end;


procedure TForm1.wyszukaj(lewo, prawo, poczym: integer; var pocz,kon : integer; kryterium : rekord );     // wyszukiwanie binarne
var
srodek : Integer ;
x : rekord;
begin



while lewo <= prawo do
   begin
      srodek := (lewo + prawo) div 2;
      x := tab[srodek];

      if porownaj(x, kryterium, poczym) = 0 then
         begin
	 break;
         end;

      if porownaj(x, kryterium, poczym) = 1 then
         lewo := srodek + 1
      else
         prawo := srodek - 1;
   end;

//szukanie powyzej i ponizej takich smaych elementow
if lewo <= prawo then begin
pocz := srodek;
kon := srodek;

 while (pocz-1 >= 0) do if (porownaj(x, tab[pocz-1], poczym) = 0)then pocz:=pocz-1 else break;
 while (kon+1 <= Length(tab)-1) do if (porownaj(x, tab[kon+1], poczym) = 0) then kon:=kon+1 else break;

end;

end;






end.



